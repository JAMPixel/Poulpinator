//
// Created by max on 19/03/16.
//

#include "Carotte.hpp"


Carotte::Carotte() {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(3,6);
    std::uniform_int_distribution<> dis2((int) WIDTH*0.28, (int) WIDTH*0.70);

    indLigne = dis(gen);

    posX = dis2(gen);

}

int Carotte::GetLigne() {
    return indLigne;
}

int Carotte::GetPos() {
    return posX;
}