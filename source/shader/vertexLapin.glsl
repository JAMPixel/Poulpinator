#version 330 core
layout (location = 0) in vec3 vertice;
layout (location = 1) in vec2 uv;

uniform mat3 scale;
uniform mat3 tran;
uniform vec2 hg;
uniform vec2 bd;
out vec2 uvOut;

void main() {
    gl_Position = vec4(scale*tran*vertice,1.);
    uvOut = mix(hg,hg+bd,uv);


}
