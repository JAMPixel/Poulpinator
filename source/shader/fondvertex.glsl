#version 330 core
layout (location = 0) in vec3 vertices;
layout (location = 1) in vec2 uv;

uniform mat3 scale;
uniform mat3 tran;

out vec2 uvOut;

void main(){
    gl_Position= vec4(scale*tran*vertices,1.);

    uvOut = uv;

}