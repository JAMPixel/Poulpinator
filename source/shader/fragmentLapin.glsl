#version 330 core

out vec4 color;
uniform sampler2D texture2D;
in vec2 uvOut;

void main() {
    color = texture(texture2D,uvOut).rgba;
    if(color.rgb == vec3(0.,0.,0.)){
        color=vec4(0.,0.,1.,color.a);
    }


}
