//
// Created by max on 19/03/16.
//

#ifndef POULPINATOR_TENTACULE_HPP
#define POULPINATOR_TENTACULE_HPP

#include <memory>
#include <vector>
class Lapin;
class Tentacule{
public:
    Tentacule();

    void mvTentacule(int x);

    int attraper(int vert, std::vector <std::unique_ptr<Lapin> > &vecLapin);

    int surLapin(std::vector< std::unique_ptr<Lapin> >& vecLapin);

    void grabLapin(int nb, std::vector< std::unique_ptr<Lapin> >& vecLapin);

    //Getters et Setters
    int GetX();
    int GetY();

    bool getMvt();
    bool getCatching();
    void setMvY(int qte);
    bool mvt;
    bool hasCaught =false;
private:
    int posX;
    int posY;

    int mvY;
    bool catching;

};

#endif //POULPINATOR_TENTACULE_HPP
