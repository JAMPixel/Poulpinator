//
// Created by unnom on 20/03/16.
//

#ifndef POULPINATOR_SOUND_HPP
#define POULPINATOR_SOUND_HPP

#include <SDL2/SDL_mixer.h>

class Sound {
private :
    Mix_Music * music = nullptr;
    Mix_Chunk *son = nullptr;

public:
    Sound();
    void play(int chanel);
    ~Sound();

};


#endif //POULPINATOR_SOUND_HPP
