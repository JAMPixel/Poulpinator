//
// Created by unnom on 19/03/16.
//

#include <random>
#include <iostream>
#include "Game.h"


Game::Game():width(0),height(0),nbLigne(0),display1(width,height){

}
Game::Game(int width,int heigth, int nbLigne):width(width), height(heigth), nbLigne(nbLigne),display1(width,height){

    std::random_device r;

    // Choose a random mean between 1 and 6
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(0, 1600);
    int mean = uniform_dist(e1);

    for (int i = 0; i < 10 ; i++ ){
        theCarots.push_back(std::make_unique<Carotte>());
        ligneDispo[theCarots[i]->GetLigne()] = 1;
    }
    tentacules.emplace_back();
    tentacules.emplace_back();

}

void Game::display(int frame) {
    display1.displaysFond(display1.fond);
    display1.displayTerre(display1.fond);
    for(auto &carrot : theCarots){
        display1.displayCarot(display1.fond, *carrot);
    }
    for(auto &tentacule : tentacules)
    display1.displayTentacule(display1.rabbit, tentacule, grab, frameGrab);

    for (int i =0 ;i< theRabbit.size();i++){
        display1.displayLapin(display1.rabbit,*(theRabbit[i]),frame,i);
    }

    display1.displayBuisson(display1.fond);



    for(auto &zozio : thePiaf){
        display1.displayZozio(display1.rabbit, zozio, frame);
    }
    std::string afficherVie("Il reste "+std::to_string(theCarots.size()) + " carottes");
    display1.displayText(afficherVie, glm::vec2(width - afficherVie.size()*10, 0),
                         (glm::vec2(20,20)));
    std::string objectifText("Objectif: "+std::to_string(objectif)+" lapins");
    std::string etatActuel("\nVous avez mange "+std::to_string(lapinManger)+" lapins");
    display1.displayText(objectifText+etatActuel, glm::vec2(width/2 - objectifText.size()*15/2, 0),
                         (glm::vec2(30,30)));

    if (theCarots.size() == 0 ){
        std::string fin("T'as perdu  !!!!");
        display1.displayText(fin, glm::vec2(800 - fin.size() * 25 / 2, 500 - 25),
                             (glm::vec2(50,50)));

    }else if (objectif == lapinManger ){
        std::string fin("Objectif atteint");
        display1.displayText(fin, glm::vec2(800 - fin.size() * 25 / 2, 500 - 25),
                             (glm::vec2(50,50)));
    }
}

void Game::play(int frame) {
    int precFrame;

    if(inGame) {
            initLigne();
            checkLigne();
            std::random_device r;
            int nivGame =1;

            // Choose a random mean between 1 and 6
            std::default_random_engine e1(r());
            std::bernoulli_distribution bernoulliDistrib(0.1);
            bool mean = bernoulliDistrib(e1);

            if (mean && !popFini) {
                if (theCarots.size()) {
                    theRabbit.push_back(std::make_unique<Lapin>(ligneDispo,theCarots,nivGame));
                    nombreLapins++;
                }
                if (nombreLapins == objectif) {
                    popFini = true;
                    precFrame = frame;
                    nivGame++;
                    std::cout<<"lvlup"<<std::endl;
                }
            }
            if(!theRabbit.size() && (frame-precFrame)%60==5){
                std::cout<<(frame-precFrame)%60<<std::endl;
                popFini=false;
                nombreLapins=0;
            }
        }
            checkZozio();

        for (auto i = begin(theRabbit); i != end(theRabbit);) {
            (*i)->mvLapin(*this);

            //Cree par DIEU
            if (inGame) {
                int carotte = (*i)->surCarotte(this->theCarots);
                if (carotte != -1) {
                    (*i)->mangeCarotte(*(this->theCarots[carotte]));
                    theCarots.erase(begin(theCarots) + carotte);
                }

                grab = grab || (*i)->getGrabbed();
            }

                if ((*i)->GetLigne() > 10 || (*i)->GetLigne() <= 0 || (*i)->GetPos() < 0 || (*i)->GetPos() > 1600) {
                    if ((*i)->getGrabbed()) {
                        lapinManger++;
                        (*i)->caught();
                    }
                    i = theRabbit.erase(i);

                    frameGrab = 0;
                    grab = false;
                } else {
                    i++;
                }
                //Fin du divin
                if (grab) {
                    frameGrab++;
                }


        }


        if (objectif == lapinManger || theCarots.size() == 0 ){
            inGame = false;
        }
        for (auto i = begin(thePiaf); i != end(thePiaf);) {
            if ((*i).x < -50 || (*i).x > 1650 || i->fall && !((i->time)%30)){
                i = thePiaf.erase(i);
            } else {
                if(!i->dead)
                    (*i).x += (*i).sens * 1;
                else if(!(i->fall))
                    i->y += 2;
                else if(i->fall) {
                    i->time++;

                }
                if(i->y >300){
                    i->fall = true;
                }
                i++;

                //(*i).x +=1;
            }
        }
    // algo de pop de piaf
        if (thePiaf.size() < 6) {
            int sens, x, y;
            int pas = width/3;
            std::random_device s;
            // Choose a random mean between 1 and 6
            std::default_random_engine e2(s());
            std::bernoulli_distribution graou(0.5);
            bool plop = graou(e2);
            if (plop) {
                sens = 1;
                x = -20;
            } else {
                sens = -1;
                x = 1620;
            }
            y = 50 * graou(e2);
            for (auto &piaf : thePiaf){
                if( piaf.x == pas || piaf.x == 2*pas ){
                    thePiaf.push_back(Zozio{x, y, sens, 0});
                    break;
                }
            }
            if(thePiaf.size() < 3 && !(frame % 120)){
                thePiaf.push_back(Zozio{x, y, sens, 0});
            }


        }


}



void Game::initLigne() {
    for (int i = 0; i < 10 ;i++){
        ligneDispo[i] = 0;
    }
}
void Game::checkLigne(){
    for (int i = 0; i < theCarots.size() ;i++){
        ligneDispo[theCarots[i]->GetLigne()] = 1;
    }
}

void Game::checkZozio() {
    for(auto & zozio : thePiaf){
        for( auto & cretin : theRabbit){
            if(cretin->GetLigne()*120-50 < zozio.y+30 && cretin->GetPos()< zozio.x && cretin->GetPos()+50 > zozio.x){
                zozio.dead = true;
                break;
            }
        }
    }
}
