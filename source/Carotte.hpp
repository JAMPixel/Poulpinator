//
// Created by max on 19/03/16.
//

#ifndef PROJECT_CAROTTE_HPP
#define PROJECT_CAROTTE_HPP

#include <iostream>
#include <random>
#define NB_LIGNES 6
#define WIDTH 1600

class Carotte{
public:
    Carotte();

    //Getters
    int GetLigne();
    int GetPos();

private:
    int indLigne;
    int posX;
};


#endif //PROJECT_CAROTTE_HPP
