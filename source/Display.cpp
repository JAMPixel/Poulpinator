//
// Created by unnom on 19/03/16.
//

#include "Display.hpp"
#include <GLifier/quad.hpp>
Display::Display(int width, int height):width(width), height(height){

    vao.bind();
    //vboRect = libgl::Vbo(0,3,libgl::mat::rect);
    glGenBuffers(1,&vbo); libgl::getError();
    glBindBuffer(GL_ARRAY_BUFFER,vbo); libgl::getError();
    auto a = libgl::mat::rect.data();
    auto b = libgl::mat::rect.size()*sizeof(std::vector<glm::vec3>);
    glBufferData(GL_ARRAY_BUFFER,libgl::mat::rect.size()*sizeof(std::vector<glm::vec3>)*3,libgl::mat::rect.data(),GL_STATIC_DRAW);
    libgl::getError();
    glEnableVertexAttribArray(0);
    libgl::getError();
    glVertexAttribPointer(
            0,
            3,
            GL_FLOAT,
            GL_FALSE,
            0,
            (void *)0
    );libgl::getError();

    vboUv = libgl::Vbo(1,2,libgl::mat::uv);

}
void Display::displayText(std::string text, glm::vec2 pos, glm::vec2 size) {
    libgl::display(plop,text,pos,size);

}