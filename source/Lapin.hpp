//
// Created by max on 19/03/16.
//

#ifndef PROJECT_LAPIN_HPP
#define PROJECT_LAPIN_HPP


#define V_LAPIN 1


#include <iostream>
#include <random>

#include <memory>

struct Game;
class Carotte;

class Lapin{
public:
    Lapin(int ligneDispo[]);
    Lapin(int ligneDispo[], std::vector<std::unique_ptr<Carotte>>& vectCar, int nivGame);

    void mvLapin(Game&);

    void meurt(Game&);

    int surCarotte(std::vector<std::unique_ptr<Carotte> > & vectCar);

    void mangeCarotte(Carotte& carotte);

    /*void esquive();*/


    //Getters
    int GetPos();
    float GetLigne();
    int GetSens();
    int GetMvt();
    bool getGrabbed();
    bool getSayan();

    void SetMvt(int mvt);
    void setGrabbed(bool test);
    void setLigne(float qte);
    bool getSound();
    std::function <void()> caught;
    std::function <void()> die;
    int posX;


private:
    float indLigne;

    int sens;
    int qteMvt;

    bool superSayanMode;
    bool grabbed;
    bool soundPlay = false;

};


#endif //PROJECT_LAPIN_HPP
