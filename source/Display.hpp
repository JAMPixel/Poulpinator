//
// Created by unnom on 19/03/16.
//

#ifndef POULPINATOR_DISPLAY_HPP
#define POULPINATOR_DISPLAY_HPP

#include <GLifier/macro.hpp>
#include <GLifier/Texture.hpp>

#include <GLifier/quad.hpp>
#include <GLifier/Vao.hpp>
#include <GLifier/Vbo.hpp>
#include <GLifier/Program.hpp>
#include "Lapin.hpp"
#include "Sound.hpp"
#include <GLifier/displayText.hpp>


/*std::string lapinVertex {"."../../source/shader/vertexLapin.glsl"};
std::string lapinFragment {"../../source/shader/fragmentLapin.glsl"};
std::string carotteVertex {"../../source/shader/vertexcarotte.glsl"};
std::string carotteFragment{"../../source/shader/fragmentcarotte.glsl"};
std::string fondVertex {"../../source/shader/vertexfond.glsl"};
std::string fondFragment {"../../source/shader/fragmentfond.glsl"};

*/
struct Zozio{
    int x;
    int y;
    int sens;
    int deplacement = 0;
    bool dead = false;
    bool fall = false;
    int time = 1;
};

class Tentacule;
struct Display {
    libgl::Vao vao;
    libgl::Vbo vboRect;
    libgl::Vbo vboUv;
    Sound sound;

    LIBGL_CREATE_PROGRAM(fond, "../../source/shader/fondvertex.glsl", "../../source/shader/fondfragment.glsl", (scale)(tran)(texture2D))
    LIBGL_CREATE_PROGRAM(rabbit, "../../source/shader/vertexLapin.glsl", "../../source/shader/fragmentLapin.glsl", (scale)(tran)(texture2D)(hg)(bd))
    LIBGL_DISPLAYTEXT(plop,"vertexText.glsl","fragmentText.glsl", "../../Resources/font.png", glm::vec2(1600,1000))




    libgl::Texture fondTexture{"../../Resources/fond.png"};
    libgl::Texture lapinTexture{"../../Resources/lapin_sprite.png"};
    libgl::Texture buissonTexture{"../../Resources/bush.png"};
    libgl::Texture tentaculeTexture{"../../Resources/spriteTentacle.png"};
    libgl::Texture zozioTexture{"../../Resources/bird.png"};
    libgl::Texture carotTexture{"../../Resources/carrot.png"};
    libgl::Texture terreTexture{"../../Resources/tranchee.png"};
    libgl::Texture superSayan{"../../Resources/SayenCretin.png"};
    libgl::Texture zozioMort{"../../Resources/deadBird.png"};
    libgl::transformStorage store;
    GLuint vbo;
    int width;
    int height;

    Display(int width, int height);
    template <typename T>
    void displaysFond(T &fond);
    template <typename T>
    void displayLapin(T &lapinProg, Lapin &lapin, int frame, int chanel);
    template <typename T>
    void displayBuisson(T &buisson);
    template <typename T>
    void displayCarot(T &carotProg, Carotte &carot);
    template <typename T>
    void displayZozio(T &zozioProg, Zozio &zozio, int frame);
    template <typename T>
    void displayTentacule(T &tentaculeProg, Tentacule &tentacule, bool isGrabed, int frame);
    template <typename T>
    void displayTerre(T &terreProg);
    void displayText(std::string text, glm::vec2 pos, glm::vec2 size);

};

#include "Display.tpp"
#endif //POULPINATOR_DISPLAY_HPP
