
#include <GLifier/InitOGL.hpp>
#include <GLifier/displayText.hpp>
#include "Game.h"

int main(){

    constexpr int width = 1600;
    constexpr int height =1000;
    bool run = true;
    SDL_Event ev;
    libgl::InitOGL ogl(width,height,"Poulpinator");
    Game game (width,height,6);
    Uint32 time;

    const Uint8 * clavier = SDL_GetKeyboardState(nullptr);
    bool appuie = false;
    int frame = 0;
    while(run){
        time = SDL_GetTicks();
        appuie = false;
        while(SDL_PollEvent(&ev)){
            switch(ev.type){
                case SDL_QUIT:
                    run = false;
                    break;
                case SDL_KEYDOWN:
                    if(ev.key.keysym.sym == SDLK_ESCAPE){
                        run = false;
                    }else if(ev.key.keysym.sym == SDLK_e){
                        appuie =true;
                    }
                    break;
            }

        }

        int x, y, code = -1;
        SDL_GetMouseState(&x, &y);
        for(int i = 0; i<game.tentacules.size(); i++) {
            code = -1;
            if (game.inGame) {
                if (!game.tentacules[i].getCatching()) {
                    game.tentacules[i].setMvY(10 * (game.tentacules[i].GetY() < 900));
                    game.tentacules[i].mvt = (game.tentacules[i].GetY() < 900);
                }

                if (appuie && !game.tentacules[i].getMvt() || game.tentacules[i].getCatching()) {
                    if(appuie && !game.tentacules[0].getCatching())
                        appuie = false;
                    code = game.tentacules[i].attraper(y, game.theRabbit);


                }

                if (code != -1) {
                    game.tentacules[i].grabLapin(code, game.theRabbit);
                    game.tentacules[i].hasCaught = true;
                }
                if (game.tentacules[i].hasCaught) {
                    game.tentacules[i].setMvY(10);
                }
                else if ((game.tentacules[i].GetY() > 900)) {
                    game.tentacules[i].setMvY(-10);
                    game.tentacules[i].mvt = (game.tentacules[i].GetY() > 900);
                }
            }
            game.tentacules[i].mvTentacule(x+(30*i));
        }


        time = SDL_GetTicks() - time;
        if(time <17){
            SDL_Delay(time);
        }
        glClear(GL_COLOR_BUFFER_BIT);
        frame++;
        game.play(frame);
        game.display(frame);

        SDL_GL_SwapWindow(ogl.getWindow());
    }

}

