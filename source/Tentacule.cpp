//
// Created by max on 19/03/16.
//

#include "Tentacule.hpp"
#include "Lapin.hpp"

#define T_LAPIN 50
#define LARGE_TENTACULE 90


Tentacule::Tentacule() {
    posX = 800;
    posY = 900;

    catching = false;
    mvt = false;
    mvY = 0;
}

void Tentacule::mvTentacule(int x) {
    if(!mvt)
        posX = x;
    posY += mvY;
    mvt = true;
}

int Tentacule::surLapin(std::vector <std::unique_ptr<Lapin> > &vecLapin) {
    for(int i = 0; i<vecLapin.size(); i++){
        if(abs(posX-vecLapin[i]->GetPos()) <= 20 && posY <= vecLapin[i]->GetLigne()*120+T_LAPIN/2 &&  posY  +20 > vecLapin[i]->GetLigne()*120+T_LAPIN/2){

            return i;
        }

    }
    return -1;
}

int Tentacule::attraper(int vert, std::vector <std::unique_ptr<Lapin> >& vecLapin) {
    int code = surLapin(vecLapin);
    if(posY > 2*120 &&  code == -1 )
    {
        mvY = -10;
        catching = true;
    }
    else{
        catching = false;
        mvY = 10;
    }
    mvt =true;
    return code;
}

void Tentacule::grabLapin(int nb, std::vector< std::unique_ptr<Lapin> >& vecLapin) {
    vecLapin[nb]->setGrabbed(true);
    vecLapin[nb]->caught = [this]{hasCaught = false;};
    vecLapin[nb]->die =[this,&vecLapin,nb]{vecLapin[nb]->setLigne( posY/120.0f);
        vecLapin[nb]->posX = posX;};
}


int Tentacule::GetX() {
    return posX;
}

int Tentacule::GetY() {
    return posY;
}

bool Tentacule::getCatching() {
    return catching;
}

bool Tentacule::getMvt() {
    return mvt;
}

void Tentacule::setMvY(int qte) {
    mvY = qte;
}