//
// Created by unnom on 19/03/16.
//

#ifndef POULPINATOR_GAME_H
#define POULPINATOR_GAME_H

#include <vector>
#include <memory>

#include "Lapin.hpp"
#include "Carotte.hpp"
#include "Display.hpp"
#include "Tentacule.hpp"
struct Zozio;
struct Game {
    std::vector<std::unique_ptr<Lapin>> theRabbit;
    std::vector<std::unique_ptr<Carotte>> theCarots;
    std::vector<Zozio> thePiaf;
    std::vector<Tentacule> tentacules;
    const int width;
    const int height;
    const int nbLigne;
    bool grab = false;
    int frameGrab = 0;
    Display display1;
    int ligneDispo[10];
    int lapinManger = 0;
    bool popFini = false;
    int nombreLapins = 0;
    int objectif = 10;
    bool inGame = true;



    Game();
    Game(int width,int heigth, int nbLigne);
    void display(int frame);
    void play(int frame);
    void initLigne();
    void checkLigne();
    void checkZozio();





};


#endif //POULPINATOR_GAME_H
