//
// Created by unnom on 20/03/16.
//

#include <exception>
#include <stdexcept>
#include "Sound.hpp"
Sound::Sound(){
    if(Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 1024)){
        throw std::runtime_error(Mix_GetError());
    }
    music = Mix_LoadMUS("../../Resources/lapin.mp3");
    if(!music){
        throw std::runtime_error(Mix_GetError());
    }
    son = Mix_LoadWAV("../../Resources/lapin.wav");

    Mix_AllocateChannels(20);
}
Sound::~Sound(){
    Mix_CloseAudio();
    Mix_FreeMusic(music);
}
void Sound::play(int chanel) {
    Mix_Volume(chanel,MIX_MAX_VOLUME/2);
    Mix_PlayChannel(chanel,son, 0);

}