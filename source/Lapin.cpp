//
// Created by max on 19/03/16.
//

#include "Lapin.hpp"
#include "Game.h"
#include <assert.h>

Lapin::Lapin(int ligneDispo[]) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(3,6);
    std::bernoulli_distribution bdis(0.5);
    do{
        indLigne = dis(gen);
    }while(!ligneDispo[(int)indLigne]);


    superSayanMode = false;
    grabbed = false;
    qteMvt = 0;

    (bdis(gen)) ? sens = 1: sens = -1;

    (sens == 1) ? posX = 0: posX = 1600;

}

Lapin::Lapin(int ligneDispo[], std::vector<std::unique_ptr<Carotte>>& vectCar, int nivGame) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(3,6);
    // Dynamic bernoulli law
    int p = (8-nivGame*1)%8;
    std::bernoulli_distribution bdis((double)(p/(double)10));
    Carotte* carrot= nullptr;
    int directionCarrot=0;

    superSayanMode = false;
    grabbed = false;
    qteMvt = 0;

    // Rabbit's line
    do{
        indLigne = dis(gen);
    }while(!ligneDispo[(int)indLigne]);

    // Carrot on line
    int i = 0;
    do{
        carrot=vectCar[i].get();
        i++;
    }while(carrot==nullptr && carrot->GetLigne() != (int)indLigne);

    assert(carrot!= nullptr && "Pas de carrotte sur la ligne - Constructeur Lapin");
    directionCarrot=800-(carrot->GetPos());

    // Carrot on the left
    if(directionCarrot>0){
        (bdis(gen)) ? sens = -1: sens = 1;
    }
    // Carrot on the right
    else{
        (bdis(gen)) ? sens = 1: sens = -1;
    }
    (sens == 1) ? posX = 0: posX = 1600;




}

int Lapin::surCarotte(std::vector<std::unique_ptr<Carotte>>& vectCar) {
    if(!grabbed) {
        for (int i = 0; i < vectCar.size(); i++) {
            if (vectCar[i]->GetLigne() == (int) indLigne && vectCar[i]->GetPos() == posX) {
                return i;
            }
        }
    }
    return -1;
}


void Lapin::mvLapin(Game& game) {
    if(posX > -10 && posX < 1700 && indLigne > 0 && indLigne < 15) {
        if (superSayanMode)
            indLigne -= 0.1;
        else if(!grabbed)
            posX += sens * V_LAPIN;
        else
            meurt(game);
    }
}

void Lapin::meurt(Game& game) {
   die();
}


void Lapin::mangeCarotte(Carotte &carotte) {
    superSayanMode = true;
}



int Lapin::GetPos() {
    return posX;
}
float Lapin::GetLigne() {
    return indLigne;
}

int Lapin::GetSens() {
    return sens;
}

int Lapin::GetMvt() {
    return qteMvt;
}

bool Lapin::getGrabbed() {
    return grabbed;
}

bool Lapin::getSayan() {
    return superSayanMode;
}

void Lapin::SetMvt(int mvt) {
    qteMvt = mvt;
}

void Lapin::setGrabbed(bool test) {
    grabbed = test;
}

void Lapin::setLigne(float qte) {
    indLigne = qte;
}

bool Lapin::getSound(){
    bool temp = soundPlay;
    soundPlay = true;
    return temp;

}