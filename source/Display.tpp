//
// Created by unnom on 19/03/16.
//

#include "Display.hpp"
#include "Lapin.hpp"
#include "Tentacule.hpp"
#include "Carotte.hpp"

template <typename T>
void Display::displaysFond(T &fond) {
    vao.bind();

    fondTexture.bindTexture();

    store.scale = glm::vec2(width,height);
    store.tran = glm::vec2(0,0);
    fond.scale = libgl::normalizedScreenSpace(width,height);
    fond.tran = libgl::transform(store,0.0f,0.0f);
    fond.texture2D = 0;

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);





}
template <typename T>
void Display::displayLapin(T &lapinProg, Lapin &lapin, int frame, int chanel) {
        vao.bind();
        if(lapin.getSayan()){
            superSayan.bindTexture();
            lapinProg.bd = glm::vec2(1.0f,1.0f);
            lapinProg.hg = glm::vec2(0.0f,0.0f);
            if(!lapin.getSound())
                sound.play(chanel);
        }else {
            lapinTexture.bindTexture();
            lapinProg.bd = glm::vec2(1.0f / 3, 1.0f);
            lapinProg.hg = glm::vec2(lapin.GetMvt() / 3.0f, 0.0f);
            if (!(frame % 15)) {
                if (lapin.GetMvt() == 2) {
                    lapin.SetMvt(0);
                } else {
                    lapin.SetMvt(lapin.GetMvt() + 1);
                }

            }
        }

        store.scale = glm::vec2(-lapin.GetSens()*50,50);
        store.tran = glm::vec2(lapin.GetPos(),lapin.GetLigne()*120);
        lapinProg.scale = libgl::normalizedScreenSpace(width,height);
        lapinProg.tran = libgl::transform(store,0.5,0.5);
        lapinProg.texture2D = 0;


        glDrawArrays(GL_TRIANGLE_STRIP,0,4);

}
template <typename T>
void Display :: displayBuisson(T &buisson){
    vao.bind();
    buissonTexture.bindTexture();
    store.scale = glm::vec2(200,120);
    buisson.texture2D = 0;
    float j = 2.5;
    for (int i = 0 ;i < 14 ;i++){
        int plouf = -80;

        store.tran = glm::vec2((i%2)*(1600-120)+plouf+(j-(int)j)*100,j*120);
        buisson.scale = libgl::normalizedScreenSpace(width,height);
        buisson.tran = libgl::transform(store);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
        if(!((i+1)%2)){
            j+=0.5;
        }
    }
}
template <typename T>
void Display::displayTentacule(T &tentaculeProg, Tentacule &tentacule, bool isGrabed, int frame) {
    vao.bind();

    tentaculeTexture.bindTexture();

    store.scale = glm::vec2(90,800);

    tentaculeProg.scale = libgl::normalizedScreenSpace(width,height);


    tentaculeProg.texture2D = 0;
    tentaculeProg.bd = glm::vec2(1.0f/3,1.0f);
   /* if(isGrabed && frame <20){
        tentaculeProg.hg = glm::vec2(1/3.0f,0.0f);
        store.tran = glm::vec2(tentacule.GetX()-50,tentacule.GetY());
    }else if(isGrabed && frame >=20){
        tentaculeProg.hg = glm::vec2(0.0f,0.0f);
        store.tran = glm::vec2(tentacule.GetX()-500,tentacule.GetY());
    }else */{
        tentaculeProg.hg = glm::vec2(2 / 3.0f, 0.0f);
        store.tran = glm::vec2(tentacule.GetX(),tentacule.GetY());
    }
    tentaculeProg.tran = libgl::transform(store,0.5f,0.0f);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
template <typename T>
void Display::displayZozio(T &zozioProg, Zozio &zozio, int frame) {
    vao.bind();
    if(zozio.dead){
        zozioMort.bindTexture();
        zozioProg.bd = glm::vec2(1.0f/2,1.0f);
        zozioProg.hg = glm::vec2(1/2.0f,0.0f);
    }

    else{
        zozioTexture.bindTexture();
        zozioProg.bd = glm::vec2(1.0f/3,1.0f);
        zozioProg.hg = glm::vec2(zozio.deplacement/3.0f,0.0f);
        if(!(frame%15)) {
            if (zozio.deplacement == 2) {
                zozio.deplacement = 0 ;
            } else {
                zozio.deplacement++;
            }
        }

    }
    if(zozio.fall){
        zozioProg.hg = glm::vec2(0.0f,0.0f);
    }

    store.scale = glm::vec2(zozio.sens*50,50);
    store.tran = glm::vec2(zozio.x,zozio.y);
    zozioProg.scale = libgl::normalizedScreenSpace(width,height);
    zozioProg.tran = libgl::transform(store);
    zozioProg.texture2D = 0;

    glDrawArrays(GL_TRIANGLE_STRIP,0,4);

}
template <typename T>
void Display::displayCarot(T &carotProg, Carotte &carot){
    vao.bind();

    carotTexture.bindTexture();

    store.scale = glm::vec2(20,20);
    store.tran = glm::vec2(carot.GetPos(),carot.GetLigne()*120);
    carotProg.scale = libgl::normalizedScreenSpace(width,height);
    carotProg.tran = libgl::transform(store,0.0f,0.0f);
    carotProg.texture2D = 0;


    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
template <typename T>
void Display::displayTerre(T &terreProg){
    vao.bind();
    terreTexture.bindTexture();
    store.scale = glm::vec2(700,40);
    terreProg.texture2D = 0;
    terreProg.scale = libgl::normalizedScreenSpace(width,height);
    for (int i = 6 ;i < 13 ;i++){
        store.tran = glm::vec2(800,i*0.5f*120);
        terreProg.tran = libgl::transform(store,0.5,0.5);
        glDrawArrays(GL_TRIANGLE_STRIP,0,4);

    }
}